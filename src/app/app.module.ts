import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { OverviewComponent } from './overview/overview.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { AddWindowComponent } from './add-window/add-window.component';
import {ReactiveFormsModule} from '@angular/forms';
import { PhoneNumberPipe } from './phone-number.pipe';
import {StorageServiceModule} from 'ngx-webstorage-service';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { DatePipe } from './date.pipe';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    OverviewComponent,
    AddWindowComponent,
    PhoneNumberPipe,
    DatePipe,
  ],
    imports: [
      BrowserModule,
      BrowserAnimationsModule,
      MatDialogModule,
      ReactiveFormsModule,
      StorageServiceModule,
      MatInputModule,
      MatFormFieldModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
