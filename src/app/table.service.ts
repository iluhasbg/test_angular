import {Injectable} from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class TableService {

  users = [{owner: 'Savannah Nguyen', endDate: '1/15/12', profits: '$328.85', losses: '$779.58', phone: '6035550123'},
    {owner: 'Jenny Wilson', endDate: '8/30/14', profits: '$948.55', losses: '$589.99', phone: '6295550129'},
    {owner: 'Annette Black', endDate: '5/30/14', profits: '$778.35', losses: '$948.55', phone: '3085550121'},
    {owner: 'Kathryn Murphy', endDate: '11/7/16', profits: '$169.43', losses: '$202.87', phone: '3025550107'},
    {owner: 'Cameron Williamson', endDate: '7/27/13', profits: '$782.01', losses: '$710.68', phone: '3035550105'},
    {owner: 'Kristin Watson', endDate: '5/19/12', profits: '$767.50', losses: '$779.58', phone: '2255550118'},
    {owner: 'Eleanor Pena', endDate: '8/21/15', profits: '$475.22', losses: '$219.78', phone: '2055550100'},
    {owner: 'Guy Hawkins', endDate: '5/27/15', profits: '$576.28', losses: '$446.61', phone: '2075550119'},
    {owner: 'Brooklyn Simmons', endDate: '3/4/16', profits: '$219.78', losses: '$928.41', phone: '5015550124'},
    {owner: 'Robert Fox', endDate: '4/4/18', profits: '$202.87', losses: '$275.43', phone: '6845550102'},
  ];

  constructor() { }

  addUser(user: any): any {
    if (JSON.parse(localStorage.getItem('Users')) == null) {
      localStorage.setItem('Users', JSON.stringify(this.users));
      this.users = JSON.parse(localStorage.getItem('Users'));
      localStorage.setItem('user', JSON.stringify(user));
      this.users.push(user);
      localStorage.setItem('Users', JSON.stringify(this.users));
    } else {
      localStorage.setItem('user', JSON.stringify(user));
      this.users.push(user);
      localStorage.setItem('Users', JSON.stringify(this.users));
    }
  }
}
