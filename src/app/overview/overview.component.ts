import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {AddWindowComponent} from '../add-window/add-window.component';
import {TableService} from '../table.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  constructor(public dialog: MatDialog, public tableService: TableService) {
  }

  ngOnInit(): void {
    this.setUsers();
  }

  openDialog(): void {
    this.dialog.open(AddWindowComponent);
  }

  setUsers(): any {
    if (JSON.parse(localStorage.getItem('Users')).length > 0) {
      this.tableService.users = JSON.parse(localStorage.getItem('Users'));
    } else {
      return this.tableService.users;
    }
  }
}
