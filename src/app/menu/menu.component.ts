import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor() { }

  options = [{
    title: 'Overview',
    icon: '../../assets/icons/Overview.png'
    }, {
    title: 'Cloud',
    icon: '../../assets/icons/Cloud.png'
    }, {
    title: 'Sketch',
    icon: '../../assets/icons/Sketch.png'
    }, {
    title: 'Experiments',
    icon: '../../assets/icons/Experiments.png'
    }, {
    title: 'Security',
    icon: '../../assets/icons/Security.png'
    }, {
    title: 'Ownership',
    icon: '../../assets/icons/Ownership.png'
    }, {
    title: 'A/B Test',
    icon: '../../assets/icons/Tests.png'
    }, {
    title: 'Colors',
    icon: '../../assets/icons/Colors.png'
    }];

  ngOnInit(): void {
  }
}
