import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TableService} from '../table.service';

@Component({
  selector: 'app-add-window',
  templateUrl: './add-window.component.html',
  styleUrls: ['./add-window.component.scss']
})
export class AddWindowComponent implements OnInit {
  form: FormGroup;
  phonePattern = '^((\\+91-?)|0)?[0-9]{10}$';
  private user: any;

  constructor(private fb: FormBuilder, public tableService: TableService) {
  }

  initForm(): void {
    this.form = this.fb.group({
      owner: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      profits: ['', [Validators.required]],
      losses: ['', [Validators.required]],
      phone: ['', [
        Validators.required,
        Validators.pattern(this.phonePattern),
        Validators.maxLength(10)]
      ],
    });
  }

  ngOnInit(): void {
    this.initForm();
  }

  onSubmit(): void{
    this.user = this.form.value;
    this.tableService.addUser(this.user);
  }
}
